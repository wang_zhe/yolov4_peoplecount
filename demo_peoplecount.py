# -*- coding: utf-8 -*-
'''
@Time          : 20/04/25 15:49
@Author        : huguanghao
@File          : demo.py
@Noice         :
@Modificattion :poeple count using yolov4 pytorch
    @Author    :wang zhe
    @Time      :
    @Detail    :
'''

# import sys
# import time
# from PIL import Image, ImageDraw
# from models.tiny_yolo import TinyYoloNet
from tool.utils import *
from tool.torch_utils import *
from tool.darknet2pytorch import Darknet
import argparse
import cv2
import numpy as np
#add in for web service api
import os
from flask import Flask, request, url_for, send_from_directory
from werkzeug.utils import secure_filename
import json

# for flask api
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = os.getcwd()
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


html = '''
    <!DOCTYPE html>
    <title>Upload File</title>
    <h1>Photo Upload</h1>
    <form method=post enctype=multipart/form-data>
         <input type=file name=file>
         <input type=submit value=upload>
    </form>
    '''



def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], "./data/abc.jpg"))
            file_url = url_for('uploaded_file', filename=filename)
            imgPath = "./data/abc.jpg"
            # img = cv2.imread(imgPath)
            # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            # print(detect_cv2(args.cfgfile, args.weightfile, args.imgfile))
            
            return str(detect_cv2(args.cfgfile, args.weightfile, args.imgfile))
    return html

"""hyper parameters"""
use_cuda = False

def detect_cv2(cfgfile, weightfile, imgfile):
    # import cv2
    # m = Darknet(cfgfile)

    # m.print_network()
    # m.load_weights(weightfile)
    # print('Loading weights from %s... Done!' % (weightfile))

    # if use_cuda:
    #     m.cuda()

    # num_classes = m.num_classes
    # if num_classes == 20:
    #     namesfile = 'data/voc.names'
    # elif num_classes == 80:
    #     namesfile = 'data/coco.names'
    # else:
    #     namesfile = 'data/x.names'
    # class_names = load_class_names(namesfile)

    img = cv2.imread(imgfile)
    sized = cv2.resize(img, (m.width, m.height))
    sized = cv2.cvtColor(sized, cv2.COLOR_BGR2RGB)

    # for i in range(2):
    #     start = time.time()
    #     boxes = do_detect(m, sized, 0.4, 0.6, use_cuda)
    #     finish = time.time()
    #     if i == 1:
    #         print('%s: Predicted in %f seconds.' % (imgfile, (finish - start)))
    boxes = do_detect(m, sized, 0.4, 0.6, use_cuda)
    
    return plot_boxes_cv2(img, boxes[0], savename='predictions.jpg', class_names=class_names)
    

def detect_cv2_camera(cfgfile, weightfile):
    #import cv2
    m = Darknet(cfgfile)

    m.print_network()
    m.load_weights(weightfile)
    print('Loading weights from %s... Done!' % (weightfile))

    if use_cuda:
        m.cuda()

    cap = cv2.VideoCapture(0)
    # cap = cv2.VideoCapture("./test.mp4")
    cap.set(3, 1280)
    cap.set(4, 720)
    print("Starting the YOLO loop...")

    num_classes = m.num_classes
    if num_classes == 20:
        namesfile = 'data/voc.names'
    elif num_classes == 80:
        namesfile = 'data/coco.names'
    else:
        namesfile = 'data/x.names'
    class_names = load_class_names(namesfile)

    while True:
        ret, img = cap.read()
        sized = cv2.resize(img, (m.width, m.height))
        sized = cv2.cvtColor(sized, cv2.COLOR_BGR2RGB)

        start = time.time()
        boxes = do_detect(m, sized, 0.4, 0.6, use_cuda)
        finish = time.time()
        print('Predicted in %f seconds.' % (finish - start))

        result_img = plot_boxes_cv2(img, boxes[0], savename=None, class_names=class_names)

        cv2.imshow('Yolo demo', result_img)
        cv2.waitKey(1)

    cap.release()


def detect_skimage(cfgfile, weightfile, imgfile):
    from skimage import io
    from skimage.transform import resize
    m = Darknet(cfgfile)

    m.print_network()
    m.load_weights(weightfile)
    print('Loading weights from %s... Done!' % (weightfile))

    if use_cuda:
        m.cuda()

    num_classes = m.num_classes
    if num_classes == 20:
        namesfile = 'data/voc.names'
    elif num_classes == 80:
        namesfile = 'data/coco.names'
    else:
        namesfile = 'data/x.names'
    class_names = load_class_names(namesfile)

    img = io.imread(imgfile)
    sized = resize(img, (m.width, m.height)) * 255

    for i in range(2):
        start = time.time()
        boxes = do_detect(m, sized, 0.4, 0.4, use_cuda)
        finish = time.time()
        if i == 1:
            print('%s: Predicted in %f seconds.' % (imgfile, (finish - start)))

    plot_boxes_cv2(img, boxes, savename='predictions.jpg', class_names=class_names)


def get_args():
    parser = argparse.ArgumentParser('Test your image or video by trained model.')
    parser.add_argument('-cfgfile', type=str, default='./cfg/yolov4.cfg',
                        help='path of cfg file', dest='cfgfile')
    parser.add_argument('-weightfile', type=str,
                        default='yolov4.weights',
                        help='path of trained model.', dest='weightfile')
    parser.add_argument('-imgfile', type=str,
                        default='./data/abc.jpg',
                        help='path of your image file.', dest='imgfile')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = get_args()
    # for flask
    m = Darknet(args.cfgfile)
        
    m.print_network()
    m.load_weights(args.weightfile)
    print('Loading weights from %s... Done!' % (args.weightfile))
    if use_cuda:
        m.cuda()

    num_classes = m.num_classes
    if num_classes == 20:
        namesfile = 'data/voc.names'
        print('voc.names used')
    elif num_classes == 80:
        namesfile = 'data/coco.names'
        print('coco.names used')
    else:
        namesfile = 'data/x.names'
    class_names = load_class_names(namesfile)
    
    app.run(host='0.0.0.0', port= 5000)
    print('---------------------------------')
    if args.imgfile:
        
        # detect_cv2(args.cfgfile, args.weightfile, args.imgfile)
        print('1111111111111111111111111')
        # detect_imges(args.cfgfile, args.weightfile)
        # detect_cv2(args.cfgfile, args.weightfile, args.imgfile)
        # detect_skimage(args.cfgfile, args.weightfile, args.imgfile)
    else:
        detect_cv2_camera(args.cfgfile, args.weightfile)
